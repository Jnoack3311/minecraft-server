FROM openjdk:17-slim-buster
COPY . /server

RUN apt-get update && apt-get install python3 python3-pip -y
RUN pip3 install mcstatus

WORKDIR /server/
ENTRYPOINT ["./start-server.sh"]