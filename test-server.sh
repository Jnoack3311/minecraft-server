#!/bin/bash
./start-server.sh &

for ((i = 0; i < 60; ++i)); do
    if mcstatus 0.0.0.0 ping > /dev/null; then
      exit 0
    fi
    sleep 10
done

exit 1
